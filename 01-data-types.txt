/* Microlang file (uLang): '*.ulang' */

a : integer;					// OK: a = 0
a : integer = 2;				// OK: a = 2
a : integer; a = 2;				// OK: a = 2
a;								// FAIL

b : real = a + 2 * (a - 1);		// OK: b = 4.0

c : integer;	// default: c = 0
d : real;		// default: d = 0.0
e : string;		// default: e = ""
f : char;		// default: f = '\0'
g : logical;	// default: g = false
h : complex;	// default: h = 0+0i

i : date;		// default: i = 1970-01-01
j : datetime;	// default: j = 1970-01-01_00:00:00
k : time;		// default: k = 00:00:00
l : microtime;	// default: l = 00:00:00.000

m : regex;		// default: m = null (It must be initialized anyway!)

n : regex = /([A-Z])\w+/gi;


records : array(integer); 				// default: records = []
records : array(integer) = [1, 2, 3];
// tip: array index starts at 1, so the first element is a[1], the second one is a[2], and so on

records : dict(string, integer);		// default: records = {}
records : dict(string, integer) = {
	"foo" : 0, 
	"bar" : 1, 
	"baz" : 2
};
records : dict(string, integer) = {		// another way
	foo : 0, 
	bar : 1, 
	baz : 2
};

score : tuple(string, real, integer) = ("foo", 1.0, 2);
scores : array(tuple(integer, string, real)) = [
	(1, "foo", 1.0),
	(2, "bar", 2.1),
	(3, "baz", 3.2)
];
